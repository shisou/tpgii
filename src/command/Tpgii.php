<?php

namespace shisou\tpgii\command;

use shisou\tpgii\handler\DataBase;
use think\console\Input;
use think\console\Output;
use think\console\Command;
use shisou\tpgii\handler\Sql;
use shisou\tpgii\handler\Model;
use shisou\tpgii\handler\DatabaseUpdater;
use think\console\input\Option;
use think\console\input\Argument;
use shisou\tpgii\handler\ParseFile;
use shisou\tpgii\handler\FastAdmin;
use shisou\tpgii\handler\ParseExcel;

class Tpgii extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('model')
            ->addArgument('action', Argument::OPTIONAL, "action")
            ->addOption('name', 't', Option::VALUE_OPTIONAL, 'table name')
            ->setDescription('automatically generate model');
    }

    /**
     * @param Input  $input
     * @param Output $output
     *
     * @return int|void|null
     */
    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

        if (!env('APP_DEBUG')) {
            $output->writeln('DEBUG IS FALSE');

            return false;
        }

        $action = $input->getArgument('action');
        if ($input->hasOption('name')) {
            $name = $input->getOption('name');
        }
        $tables = (new ParseExcel())->get();

        if (!$action) {
            (new Sql($tables))->run();
            (new DataBase($tables))->run();
            (new Model($tables))->run();
        }

        if ($action == 'dataSql') {
            (new DataBase($tables))->dataSql();
        }

        if ($action == 'sql') {
            (new Sql($tables))->run();
            (new DataBase($tables))->run();
        }

        if ($action == 'model') {
            (new Model($tables))->run();
        }
        // 指令输出
        $output->writeln('Run complete');
    }
}
