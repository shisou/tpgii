<?php

namespace shisou\tpgii\controller;

class _Controller
{
    public function __construct()
    {
        define("GII_VIEW_PATH", __DIR__ . "/../view/tpgii.html");
    }
}
